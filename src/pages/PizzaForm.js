import Router from '../Router.js';
import Page from './Page.js';

export default class AddPizzaPage extends Page {
	render() {
		return /*html*/ `
			<form class="pizzaForm">
				<label>
					Nom :
					<input type="text" name="name">
				</label>
				<label>
					Image :<br/>
					<input type="text" name="image" placeholder="https://source.unsplash.com/xxxxxxx/600x600">
					<small>Vous pouvez trouver des images de pizza sur <a href="https://unsplash.com/">https://unsplash.com/</a> puis utiliser l'URL <code>https://source.unsplash.com/xxxxxxx/600x600</code> où <code>xxxxxxx</code> est l'id de la photo choisie (celui dans la barre d'adresse)</small>
				</label>
				<label>
					Prix petit format :
					<input type="number" name="price_small" step="0.05">
				</label>
				<label>
					Prix grand format :
					<input type="number" name="price_large" step="0.05">
				</label>

				<button type="submit">Ajouter</button>
			</form>`;
	}

	mount(element) {
		super.mount(element);
		const form = this.element.querySelector('.pizzaForm');
		form.addEventListener('submit', event => {
			event.preventDefault();
			this.submit();
		});
	}

	submit() {
		// D.4. La validation de la saisie
		const nameInput = this.element.querySelector('input[name="name"]'),
			name = nameInput.value;
		if(!this.verifyName(name)) return;

		const imageInput = this.element.querySelector('input[name="image"]'),
			image = imageInput.value;
		if(!this.verifyImage(image)) return;
		
		const price_smallInput = this.element.querySelector('input[name="price_small"]'),
			price_small = price_smallInput.value;
		if(!this.verifyPriceSmall(price_small)) return;
		
		const price_largeInput = this.element.querySelector('input[name="price_large"]'),
			price_large = price_largeInput.value;
		if(!this.verifyPriceLarge(price_large)) return;



		this.sendPizza(name, image, price_small, price_large);
		alert(`La pizza ${name} a été ajoutée !`);
		nameInput.value = '';
		Router.navigate('/');
	}

	verifyName(name){
		if (name === '') {
			alert('Erreur : le champ "Nom" est obligatoire');
			return false;
		}
		return true;
	}

	verifyImage(image){
		if (image === '') {
			alert('Erreur : le champ "Image" est obligatoire');
			return false;
		}
		return true;
	}

	verifyPriceSmall(price_small){
		if (price_small === '') {
			alert('Erreur : le champ "Price Small" est obligatoire');
			return false;
		}
		return true;
	}

	verifyPriceLarge(price_large){
		if (price_large === '') {
			alert('Erreur : le champ "Price Large" est obligatoire');
			return false;
		}
		return true;
	}

	sendPizza(name, image, price_small, price_large){
		fetch(
			'http://localhost:8080/api/v1/pizzas',
			{
				method:'POST',
				headers: { 'Content-Type': 'application/json' },
				body: JSON.stringify({name: name, image: image, price_small: price_small, price_large: price_large})
			}
		);
		
	}
}
